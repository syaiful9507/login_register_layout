package com.example.calculatorpens;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button ChangeFrag;
    Boolean kondisi = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChangeFrag  =   (Button)findViewById(R.id.change_button);
        ChangeFrag.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case  R.id.change_button:
                if (kondisi) {
                    kondisi = false;
                    FirstFrag satu  =   new FirstFrag();
                    FragmentManager FM = getSupportFragmentManager();
                    FragmentTransaction FT = FM.beginTransaction();
                    FT.replace(R.id.main_menu, satu);
                    FT.commit();
                } else {
                    kondisi = true;
                    SecondFrag dua = new SecondFrag();
                    FragmentManager FM2 = getSupportFragmentManager();
                    FragmentTransaction FT2 = FM2.beginTransaction();
                    FT2.replace(R.id.main_menu, dua);
                    FT2.commit();
                }
                break;
        }

    }
}
