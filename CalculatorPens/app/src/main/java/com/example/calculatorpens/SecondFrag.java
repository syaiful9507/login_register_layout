package com.example.calculatorpens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class SecondFrag extends Fragment implements View.OnClickListener {

    Boolean kondisi = true;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View viewFrag2  =   inflater.inflate(R.layout.activity_second_frag, container, false);
        Button btn = (Button) viewFrag2.findViewById(R.id.btn_sign_up);
        btn.setOnClickListener(this);

        return viewFrag2;

    }


    //methode onclick

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case  R.id.btn_sign_up:
               Intent intent = new Intent(getContext(), RegisterActivity.class);
               startActivity(intent);
                break;
        }

    }






}
