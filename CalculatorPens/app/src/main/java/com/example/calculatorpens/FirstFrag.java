package com.example.calculatorpens;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class  FirstFrag extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewFrag1  =   inflater.inflate(R.layout.activity_first_frag, container, false);
        return viewFrag1;
    }
}
